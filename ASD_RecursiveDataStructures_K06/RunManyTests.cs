﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_RecursiveDataStructures_K06
{
    internal static class RunManyTests
    {
        public static void ToCsv(IRecursiveDataStructure rds,Dictionary<int,int> dictionary,int subsetLength, int numberOfTests, string filename) {
            StringBuilder csv = new StringBuilder();
            IRecursiveDataStructure rdsCopy = (IRecursiveDataStructure)rds.Clone();
            for (int i = 0; i < numberOfTests; i++)
            {
                List<double> result400 = Test.Run(rds, dictionary, subsetLength);
                var temp = string.Join(";", result400.Select(x => x.ToString()).ToArray());
                csv.Append(temp + "\n");
                rds = (IRecursiveDataStructure)rdsCopy.Clone();
            }
            string filePath = filename;

            // Write the resultString to the file
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.WriteLine(csv);
            }
        }
    }
}
