﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_RecursiveDataStructures_K06
{
    internal static class SubsetGenerator
    {
        public static Dictionary<int, int> GenerateSubset(Dictionary<int, int> fullDictionary, int subsetSize = 200)
        {
            if (fullDictionary.Count < subsetSize)
            {
                // Jeśli słownik ma mniej niż 200 elementów, zwróć cały słownik
                return new Dictionary<int, int>(fullDictionary);
            }

            // Przygotuj losowy generator
            var random = new Random();

            // Utwórz listę kluczy z pełnego słownika
            var keys = fullDictionary.Keys.ToList();

            // Losowo wybierz klucze do podzbioru
            var selectedKeys = new HashSet<int>();
            while (selectedKeys.Count < subsetSize)
            {
                var randomIndex = random.Next(0, keys.Count);
                selectedKeys.Add(keys[randomIndex]);
            }

            // Utwórz podzbiór na podstawie wybranych kluczy
            var subset = new Dictionary<int, int>();
            foreach (var key in selectedKeys)
            {
                subset[key] = fullDictionary[key];
            }

            return subset;
        }
    }
}
