﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_RecursiveDataStructures_K06
{
    public class BinarySearchTree : IRecursiveDataStructure
    {
        private class Node
        {
            public int Key;
            public int Value;
            public Node Less;
            public Node Greater;

            public Node this[int key]
            {
                get
                {
                    if (key < Key)
                    {
                        return Less;
                    }
                    if (key > Key)
                    {
                        return Greater;
                    }
                    return null;
                }
                set
                {
                    if (key < Key)
                    {
                        Less = value;
                        return;
                    }
                    if (key > Key)
                    {
                        Greater = value;
                        return;
                    }
                    throw new DuplicateKeyException();
                }
            }

            public void ForEachR(Action<int, int> action)
            {
                if (Less != null)
                    Less.ForEachR(action);
                action(Key, Value);
                if (Greater != null)
                    Greater.ForEachR(action);
            }

            public Node Clone()
            {
                return new Node { Key = Key, Value = Value, Greater = Greater, Less = Less };
            }
        }

        private Node root = new Node { Key = int.MinValue, Less = null, Greater = null };

        public int this[int key] 
        {
            get 
            {
                Node parent = root;
                while (parent[key] != null && parent[key].Key != key)
                {
                    parent = parent[key];
                }
                if (parent[key] == null)
                {
                    throw new KeyNotFoundException();
                }
                return parent[key].Value;
            }
            set
            {
                Node parent = root;
                while (parent[key] != null && parent[key].Key != key)
                {
                    parent = parent[key];
                }
                if (parent[key] == null)
                {
                    throw new KeyNotFoundException();
                }
                parent[key].Value = value;
            }
        }

        public void Add(int key, int value)
        {
            Node parent = root;
            while (parent[key] != null && parent[key].Key != key)
            {
                parent = parent[key];
            }
            if (parent[key] != null && parent[key].Key == key)
            {
                // klucz istnieje - nie ma sensu dodawać go do struktury
                return;
            }
            parent[key] = new Node { Key = key, Value = value };
        }

        public void ForEach(Action<int, int> action)
        {
            Stack<Node> nodes = new Stack<Node>();
            if (root[0] != null)
                nodes.Push(root[0].Clone());
            while (nodes.Count > 0)
            {
                Node current = nodes.Pop();
                if (current.Less != null)
                {
                    nodes.Push(current);
                    nodes.Push(current.Less.Clone());
                    current.Less = null;
                    continue;
                }
                action(current.Key, current.Value);
                if (current.Greater != null)
                {
                    nodes.Push(current.Greater.Clone());
                }
            }
        }

        public void ForEachR(Action<int, int> action)
        {
            if (root[0] != null)
            {
                root[0].ForEachR(action);
            }
        }

        public void Remove(int key)
        {
            Node parent = root;
            while (parent[key] != null && parent[key].Key != key)
            {
                parent = parent[key];
            }
            if (parent[key] == null) 
            {
                return;
            }
            if (parent[key].Less == null || parent[key].Greater == null)
            {
                Node child = parent[key].Less ?? parent[key].Greater;
                parent[key] = child;
                return;
            }
            Node max = parent[key].Less;
            while (max.Greater != null)
            {
                max = max.Greater;
            }
            Remove(max.Key);
            parent[key].Key = max.Key;
            parent[key].Value = max.Value;
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
