﻿// See https://aka.ns\new-console-template for more information
using ASD_RecursiveDataStructures_K06;
using System.Collections.Generic;
using System.Text;
namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            IDictionaryGenerator random = new RandomKeyValue();
            IDictionaryGenerator ascending = new RandomAscendingValues();
            IDictionaryGenerator descending = new RandomDescendingValues();
            IDictionaryGenerator vshape = new RandomKeyValueVShape();
            Dictionary<int, int> random400 = random.Next(400, 0, 200000);
            Dictionary<int, int> ascending400 = ascending.Next(400, 0, 200000);
            Dictionary<int, int> descending400 = descending.Next(400, 0, 200000);
            Dictionary<int, int> vshape400 = vshape.Next(400, 0, 200000);

            Dictionary<int, int> random2000 = random.Next(2000, 0, 200000);
            Dictionary<int, int> ascending2000 = ascending.Next(2000, 0, 200000);
            Dictionary<int, int> descending2000 = descending.Next(2000, 0, 200000);
            Dictionary<int, int> vshape2000 = vshape.Next(2000, 0, 200000);

            Dictionary<int, int> random7000 = random.Next(7000, 0, 200000);
            Dictionary<int, int> ascending7000 = ascending.Next(7000, 0, 200000);
            Dictionary<int, int> descending7000 = descending.Next(7000, 0, 200000);
            Dictionary<int, int> vshape7000 = vshape.Next(7000, 0, 200000);

            Dictionary<int, int> random10000 = random.Next(10000, 0, 200000);
            Dictionary<int, int> ascending10000 = ascending.Next(10000, 0, 200000);
            Dictionary<int, int> descending10000 = descending.Next(10000, 0, 200000);
            Dictionary<int, int> vshape10000 = vshape.Next(10000, 0, 200000);

            //RunManyTest.ToCsv(new typStruktury(), słownik, długość podzbioru, liczba wykonanych testów, nazwa pliku)
            //output w pliku czas dodawania; czas iteracji, czas dostępu kluczy z podzbioru, czas usuwania wedlug kluczy z podzbioru 

            RunManyTests.ToCsv(new SortedSinglyLinkedList(), random400, 200, 40, "SSLL_random_400.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), ascending400, 200, 40, "SSLL_ascending_400.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), descending400, 200, 40, "SSLL_descending_400.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), vshape400, 200, 40, "SSLL_vshape_400.txt");

            RunManyTests.ToCsv(new SortedSinglyLinkedList(), random2000, 200, 40, "SSLL_random_2000.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), ascending2000, 200, 40, "SSLL_ascending_2000.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), descending2000, 200, 40, "SSLL_descending_2000.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), vshape2000, 200, 40, "SSLL_vshape_2000.txt");

            RunManyTests.ToCsv(new SortedSinglyLinkedList(), random7000, 200, 40, "SSLL_random_7000.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), ascending7000, 200, 40, "SSLL_ascending_7000.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), descending7000, 200, 40, "SSLL_descending_7000.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), vshape7000, 200, 40, "SSLL_vshape_7000.txt");

            RunManyTests.ToCsv(new SortedSinglyLinkedList(), random10000, 200, 40, "SSLL_random_10000.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), ascending10000, 200, 40, "SSLL_ascending_10000.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), descending10000, 200, 40, "SSLL_descending_10000.txt");
            RunManyTests.ToCsv(new SortedSinglyLinkedList(), vshape10000, 200, 40, "SSLL_vshape_10000.txt");

            RunManyTests.ToCsv(new BinarySearchTree(), random400, 200, 40, "BST_random_400.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), ascending400, 200, 40, "BST_ascending_400.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), descending400, 200, 40, "BST_descending_400.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), vshape400, 200, 40, "BST_vshape_400.txt");

            RunManyTests.ToCsv(new BinarySearchTree(), random2000, 200, 40, "BST_random_2000.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), ascending2000, 200, 40, "BST_ascending_2000.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), descending2000, 200, 40, "BST_descending_2000.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), vshape2000, 200, 40, "BST_vshape_2000.txt");

            RunManyTests.ToCsv(new BinarySearchTree(), random7000, 200, 40, "BST_random_7000.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), ascending7000, 200, 40, "BST_ascending_7000.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), descending7000, 200, 40, "BST_descending_7000.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), vshape7000, 200, 40, "BST_vshape_7000.txt");

            RunManyTests.ToCsv(new BinarySearchTree(), random10000, 200, 40, "BST_random_10000.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), ascending10000, 200, 40, "BST_ascending_10000.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), descending10000, 200, 40, "BST_descending_10000.txt");
            RunManyTests.ToCsv(new BinarySearchTree(), vshape10000, 200, 40, "BST_vshape_10000.txt");

            RunManyTests.ToCsv(new AVLTree(), random2000, 200, 40, "AVL_random_2000.txt");
            RunManyTests.ToCsv(new AVLTree(), ascending2000, 200, 40, "AVL_ascending_2000.txt");
            RunManyTests.ToCsv(new AVLTree(), descending2000, 200, 40, "AVL_descending_2000.txt");
            RunManyTests.ToCsv(new AVLTree(), vshape2000, 200, 40, "AVL_vshape_2000.txt");

            RunManyTests.ToCsv(new AVLTree(), random7000, 200, 40, "AVL_random_7000.txt");
            RunManyTests.ToCsv(new AVLTree(), ascending7000, 200, 40, "AVL_ascending_7000.txt");
            RunManyTests.ToCsv(new AVLTree(), descending7000, 200, 40, "AVL_descending_7000.txt");
            RunManyTests.ToCsv(new AVLTree(), vshape7000, 200, 40, "AVL_vshape_7000.txt");

            RunManyTests.ToCsv(new AVLTree(), random10000, 200, 40, "AVL_random_10000.txt");
            RunManyTests.ToCsv(new AVLTree(), ascending10000, 200, 40, "AVL_ascending_10000.txt");
            RunManyTests.ToCsv(new AVLTree(), descending10000, 200, 40, "AVL_descending_10000.txt");
            RunManyTests.ToCsv(new AVLTree(), vshape10000, 200, 40, "AVL_vshape_10000.txt");
        }
    }
}