﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_RecursiveDataStructures_K06
{
    public class RandomAscendingValues : IDictionaryGenerator
    {
        public Dictionary<int, int> Next(int keys, int minValue, int maxValue)
        {
            if (keys > maxValue) throw new Exception("Ascending: keys>maxValue");
            Dictionary<int, int> dict = new Dictionary<int, int>();
            Random rng = new Random();
            var randomValues = new List<int>();
            int temp;
            for (int i = 0; i < keys; i++)
            {
                while (randomValues.Contains(temp = rng.Next(minValue, maxValue)));
                randomValues.Add(temp);
            }
            randomValues= randomValues.OrderBy(i=>i).ToList();
            for (int i = 0; i < keys; i++)
            {
                int key = i;
                int value = randomValues[i]; // Zapewnienie, że nie wyjdziemy poza zakres

                dict.Add(key, value);
            }
            return dict;
        }
    }
}