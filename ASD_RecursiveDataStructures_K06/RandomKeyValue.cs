﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_RecursiveDataStructures_K06
{
    public class RandomKeyValue : IDictionaryGenerator
    {
        public Dictionary<int,int> Next(int keys, int minValue, int maxValue) {
            Dictionary<int, int> dict = new Dictionary<int, int>();
            Random rng  = new Random();
            HashSet<int> usedValues = new HashSet<int>();
            for (int i = 0; i < keys; i++)
            {
                int key;

                // Generowanie unikalnego klucza
                do
                {
                    key = rng.Next(0, keys); // zakres kluczy od 1 do maxKey
                } while (dict.ContainsKey(key));

                int value;

                // Generowanie unikalnej wartości
                do
                {
                    value = rng.Next(minValue, maxValue); // zakres wartości od 1 do 99
                } while (usedValues.Contains(value));

                dict.Add(key, value);
                usedValues.Add(value);

                //Console.WriteLine($"Dodano: {key}, {value}");
                
            }
            return dict;
        }
    }
}
