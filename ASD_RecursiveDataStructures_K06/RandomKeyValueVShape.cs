﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_RecursiveDataStructures_K06
{
    public class RandomKeyValueVShape : IDictionaryGenerator
    {
        public Dictionary<int, int> Next(int keys, int minValue, int maxValue)
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();
            Random rng = new Random();
            HashSet<int> usedValues = new HashSet<int>();
            int midPoint = keys / 2;
            var potentialValues = Enumerable.Range(minValue, maxValue - minValue + 1).ToList();
            var shuffledValues = potentialValues.OrderBy(x => rng.Next()).ToList();

            // Pobieramy wartości dla malejącej części V
            var descendingValues = shuffledValues.Take(midPoint).OrderByDescending(x => x).ToList();
            // Pobieramy wartości dla rosnącej części V
            var ascendingValues = shuffledValues.Skip(midPoint).Take(keys - midPoint).OrderBy(x => x).ToList();
            // Łączymy obie listy, aby utworzyć kształt V
            var vShapeValues = descendingValues.Concat(ascendingValues).ToList();

            for (int i = 0; i < keys; i++)
            {
                int key = i;
                int value = vShapeValues[i];

                dict.Add(key, value);
            }
            return dict;
        }
    }
}