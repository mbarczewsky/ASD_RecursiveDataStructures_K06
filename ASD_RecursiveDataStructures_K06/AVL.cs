﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_RecursiveDataStructures_K06
{
    public class AVLTree : IRecursiveDataStructure
    {
        private class Node
        {
            public int Key;
            public int Value;
            public int Height;
            public Node Left;
            public Node Right;

            public Node(int key, int value)
            {
                Key = key;
                Value = value;
                Height = 1;
            }
        }

        private Node root;

        // Helper to get the height of the tree
        private int Height(Node node)
        {
            return node == null ? 0 : node.Height;
        }

        // Helper to get the balance factor of the tree
        private int GetBalance(Node node)
        {
            if (node == null)
                return 0;
            return Height(node.Left) - Height(node.Right);
        }

        // Right rotation
        private Node RightRotate(Node y)
        {
            Node x = y.Left;
            Node T2 = x.Right;
            x.Right = y;
            y.Left = T2;
            y.Height = Math.Max(Height(y.Left), Height(y.Right)) + 1;
            x.Height = Math.Max(Height(x.Left), Height(x.Right)) + 1;
            return x;
        }

        // Left rotation
        private Node LeftRotate(Node x)
        {
            Node y = x.Right;
            Node T2 = y.Left;
            y.Left = x;
            x.Right = T2;
            x.Height = Math.Max(Height(x.Left), Height(x.Right)) + 1;
            y.Height = Math.Max(Height(y.Left), Height(y.Right)) + 1;
            return y;
        }

        // Implementing the Add method from the interface
        public void Add(int key, int value)
        {
            root = Insert(root, key, value);
        }

        private Node Insert(Node node, int key, int value)
        {
            if (node == null)
                return new Node(key, value);

            if (key < node.Key)
                node.Left = Insert(node.Left, key, value);
            else if (key > node.Key)
                node.Right = Insert(node.Right, key, value);
            else
                return node;  // Duplicate key not allowed

            node.Height = 1 + Math.Max(Height(node.Left), Height(node.Right));

            int balance = GetBalance(node);

            // Balancing the tree
            if (balance > 1 && key < node.Left.Key)
                return RightRotate(node);

            if (balance < -1 && key > node.Right.Key)
                return LeftRotate(node);

            if (balance > 1 && key > node.Left.Key)
            {
                node.Left = LeftRotate(node.Left);
                return RightRotate(node);
            }

            if (balance < -1 && key < node.Right.Key)
            {
                node.Right = RightRotate(node.Right);
                return LeftRotate(node);
            }

            return node;
        }

        // Implementing the Remove method from the interface
        public void Remove(int key)
        {
            root = Remove(root, key);
        }

        // Helper method for remove
        private Node Remove(Node node, int key)
        {
            if (node == null)
                return node;

            if (key < node.Key)
                node.Left = Remove(node.Left, key);
            else if (key > node.Key)
                node.Right = Remove(node.Right, key);
            else
            {
                if ((node.Left == null) || (node.Right == null))
                {
                    Node temp = node.Left ?? node.Right;
                    if (temp == null)
                    {
                        temp = node;
                        node = null;
                    }
                    else
                        node = temp;
                }
                else
                {
                    Node temp = MinValueNode(node.Right);
                    node.Key = temp.Key;
                    node.Value = temp.Value;
                    node.Right = Remove(node.Right, temp.Key);
                }
            }

            if (node == null)
                return node;

            node.Height = Math.Max(Height(node.Left), Height(node.Right)) + 1;
            int balance = GetBalance(node);

            if (balance > 1 && GetBalance(node.Left) >= 0)
                return RightRotate(node);

            if (balance > 1 && GetBalance(node.Left) < 0)
            {
                node.Left = LeftRotate(node.Left);
                return RightRotate(node);
            }

            if (balance < -1 && GetBalance(node.Right) <= 0)
                return LeftRotate(node);

            if (balance < -1 && GetBalance(node.Right) > 0)
            {
                node.Right = RightRotate(node.Right);
                return LeftRotate(node);
            }

            return node;
        }

        // Helper to find the node with the minimum key value found in the tree
        private Node MinValueNode(Node node)
        {
            Node current = node;
            while (current.Left != null)
                current = current.Left;
            return current;
        }

        // Implementing the indexer from the interface
        public int this[int key]
        {
            get
            {
                Node node = root;
                while (node != null)
                {
                    if (key < node.Key)
                        node = node.Left;
                    else if (key > node.Key)
                        node = node.Right;
                    else
                        return node.Value;  // Key found
                }
                throw new KeyNotFoundException("Key not found");
            }
            set
            {
                Node node = root;
                while (node != null)
                {
                    if (key < node.Key)
                        node = node.Left;
                    else if (key > node.Key)
                        node = node.Right;
                    else
                    {
                        node.Value = value;  // Key found and value updated
                        return;
                    }
                }
                throw new KeyNotFoundException("Key not found");
            }
        }

        // Implementing ForEach from the interface
        public void ForEach(Action<int, int> action)
        {
            if (root != null) InOrderIterative(root, action);
        }

        private void InOrderIterative(Node root, Action<int, int> action)
        {
            if (root == null) return;
            Stack<Node> stack = new Stack<Node>();
            Node current = root;

            while (stack.Count > 0 || current != null)
            {
                while (current != null)
                {
                    stack.Push(current);
                    current = current.Left;
                }

                current = stack.Pop();
                action(current.Key, current.Value);
                current = current.Right;
            }
        }

        // Implementing ForEachR from the interface
        public void ForEachR(Action<int, int> action)
        {
            InOrderRecursive(root, action);
        }

        private void InOrderRecursive(Node node, Action<int, int> action)
        {
            if (node == null) return;
            InOrderRecursive(node.Left, action);
            action(node.Key, node.Value);
            InOrderRecursive(node.Right, action);
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
