﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_RecursiveDataStructures_K06
{
    public interface IRecursiveDataStructure
    {
        void Add(int key, int value);

        void Remove(int key);

        void ForEach(Action<int, int> action);
        void ForEachR(Action<int, int> action);
        object Clone();
        int this[int key] { get; set; }
    }
}
