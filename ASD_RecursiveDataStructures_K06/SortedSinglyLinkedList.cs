﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_RecursiveDataStructures_K06
{
    public class SortedSinglyLinkedList : IRecursiveDataStructure
    {
        private class Node
        {
            public int key;
            public int value;
            public Node next;
        }

        private Node head = new Node { key = int.MinValue, next = null };

        public int this[int key] 
        {
            get 
            {
                var current = head;
                while (current.next != null && current.next.key < key)
                {
                    current = current.next;
                }
                if (current.next != null && current.next.key == key)
                {
                    return current.next.value;
                }
                throw new KeyNotFoundException();
            }
            set
            {
                var current = head;
                while (current.next != null && current.next.key < key)
                {
                    current = current.next;
                }
                if (current.next != null && current.next.key == key)
                {
                    current.next.value = value;
                }
            } 
        }

        //      8
        // 3, 7, 10, 


        //     5
        // 2 3
        public void Add(int key, int value)
        {
            var current = head;
            while (current.next != null && current.next.key < key)
            {
                current = current.next;
            }
            if (current.next != null && current.next.key == key)
            {
                return;
            }
            var added = new Node { key = key, value = value, next = current.next };
            current.next = added;
        }

        public void ForEach(Action<int, int> action)
        {
            var current = head;
            while (current.next != null)
            {
                action(current.next.key, current.next.value);
                current = current.next;
            }
        }

        public void ForEachR(Action<int, int> action)
        {
            RecursiveForEach(head.next, action);
        }

        private void RecursiveForEach(Node node, Action<int, int> action)
        {
            if (node != null)
            {
                action(node.key, node.value);
                RecursiveForEach(node.next, action);
            }
        }

        // 3, 5, 7
        public void Remove(int key)
        {
            var current = head;
            while (current.next != null && current.next.key < key)
            {
                current = current.next;
            }
            if (current.next != null && current.next.key == key)
            {
                current.next = current.next.next;
            }
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
