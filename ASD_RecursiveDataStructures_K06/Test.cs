﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_RecursiveDataStructures_K06
{
    internal static class Test
    {
        public static List<double> Run(IRecursiveDataStructure recursiveDataStructureType, Dictionary<int, int> dictionary, int subsetLength) {
            List<int> subset = SubsetGenerator.GenerateSubset(dictionary,subsetLength).Keys.ToList();
            IRecursiveDataStructure rds = recursiveDataStructureType;
            Stopwatch timerAdd, timerAccess, timerDelete, timerIterate;
            timerAdd = new Stopwatch();
            timerAccess = new Stopwatch();
            timerDelete = new Stopwatch();
            timerIterate = new Stopwatch();

            //czas dodawania
            timerAdd.Start();
            foreach (var kvp in dictionary)
            {
                rds.Add(kvp.Key, kvp.Value);
            }
            timerAdd.Stop();
            int k, v;
            //czas iteracji
            timerIterate.Start();
            rds.ForEachR((key,value) => { k = key; v = value; });
            timerIterate.Stop();


            //czas dostępu 
            int tempAccess;
            timerAccess.Start();
            foreach (var key in subset)
            {
                tempAccess=rds[key];
            } 
            timerAccess.Stop();


            //czas usuwania
            timerDelete.Start();
            foreach (var key in subset) 
            { 
                rds.Remove(key);
            }
            timerDelete.Stop();

            List<double> result = [
                timerAdd.Elapsed.TotalMilliseconds,
                timerIterate.Elapsed.TotalMilliseconds,
                timerAccess.Elapsed.TotalMilliseconds,
                timerDelete.Elapsed.TotalMilliseconds
                ];
            return result;
        }
    }
}